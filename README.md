# HEALTH CHECKER
Herramienta para verficacion de estados de servicios alojados en contenedores de docker



### Instalación 🔧



Para hacer build y que instale todas los requerimientos y crear el ejecutable de go

```
docker-compose build
```

Para correr el servicio 
```
docker-compose up
```
## Construido con 🛠️



* [Go](https://golang.org/) 
* [Docker](https://www.docker.com/) 
