package main

import (
	"log"
	"net/http"
	"os"
)

func StatusBack() bool {
	url := os.Getenv("URL")
	resp, err := http.Get(url) //cambiar por url del servicio que quieres verificar
	if err != nil {
		log.Println("error mientras se hacia el request", err)
		return false
	}
	if resp.StatusCode > 500 {

		log.Println("response heartbeat something i'ts wrong O-o : ", resp.StatusCode)
		return false

	}
	log.Println("response heartbeat ok : ", resp.StatusCode)
	return true
}
