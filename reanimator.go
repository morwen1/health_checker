package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

func ElectricPulse() {
	containerName := os.Getenv("CONTAINER_NAME")
	cli, err := client.NewEnvClient()
	if err != nil {
		log.Println("not found docker :/")

	}
	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}
	if len(containers) > 0 {
		for _, container := range containers {
			name := container.Names[0][1:]
			idContainer := container.ID
			fmt.Println("Name: ", name, "Id : ", idContainer)
			if name == containerName { //nombre del contenedor que quieres restartear
				cli.ContainerRestart(context.Background(), idContainer, nil)
			}

		}
	}

}

func PhoenixDown() {
	for {
		time.Sleep(80)
		heartState := StatusBack()
		if heartState == true {
			log.Println("Its alive all good :) ")
		} else {
			log.Println("oh my god we're losing him :< ")
			ElectricPulse()

		}

	}
}
