FROM golang:latest AS compiled


WORKDIR /go/src/app

COPY . . 

#RUN go get -d -v ./...

#RUN go install -v ./...

RUN go mod download
RUN go mod verify
RUN export DOCKER_API_VERSION=1.39

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o reanimator .



FROM docker:latest 
WORKDIR /
COPY --from=compiled /go/src/app/reanimator / 
#ENTRYPOINT [ "/reanimator" ]
